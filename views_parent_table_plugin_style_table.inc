<?php
/**
 * @file
 * Contains this module's extended table style plugin, which allows rows
 * from another view to be merged in.
 */

class views_parent_table_plugin_style_table extends views_plugin_style_table {
  function option_definition() {
    $options = parent::option_definition();

    $options['child_view'] = array('default' => '');
    $options['display_settings'] = array(
      'contains' => array(
        'collapsible' => array('default' => FALSE),
        'collapsed' => array('default' => FALSE),
        'target_column' => array('default' => ''),
        'singular_count' => array('default' => '1 item'),
        'plural_count' => array('default' => '@count items'),
      )
    );
    $options['column_mappings'] = array('default' => NULL);

    return $options;
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    $handlers = $this->display->handler->get_handlers('field');
    if (empty($handlers)) {
      $form['error_markup'] = array(
        '#markup' => '<div class="error messages">' . t('You need at least one field before you can configure your table settings') . '</div>',
      );
      return;
    }

    $columns = $this->sanitize_columns($this->options['columns']);
    // Create an array of allowed columns from the data we know:
    $field_names = $this->display->handler->get_field_labels();

    $fields = $this->display->handler->handlers['field'];
    $parent_column_labels = array();
    foreach ($columns as $field => $column) {
      if ($field == $column) {
        $parent_column_labels[$column] = $field_names[$column];

        // For Field API fields, a specific column needs to be selected.
        if (is_a($fields[$field], 'views_handler_field_field')) {
          $field_columns = array_keys($fields[$field]->field_info['columns']);
          foreach ($field_columns as $field_column) {
            $parent_merge_column_labels[$column . '/' . $field_column] = $field_names[$column] . ' - ' . $field_column;
          }
        }
        else {
          $parent_merge_column_labels[$column] = $field_names[$column];
        }
      }
    }

    $views = views_get_all_views();
    $child_views = array(t('None'));
    foreach ($views as $view) {
      // Don't allow a parent to be its own child. That's just weird.
      if ($view->name == $this->view->name) {
        continue;
      }
      foreach ($view->display as $display_name => $display) {
        $child_views[$view->name . '/' . $display_name] = $view->human_name . ' (Display: ' . $display->display_title . ')';
      }
    }

    // Select the child view and display.
    $form['child_view'] = array(
      '#type' => 'select',
      '#title' => t('Child view'),
      '#options' => $child_views,
      '#default_value' => $this->options['child_view'],
      '#ajax' => array(
        'path' => views_ui_build_form_url($form_state),
      ),
      '#weight' => -2,
    );

    // Create an array of allowed columns from the data we know:
    $field_names = $this->display->handler->get_field_labels();

    $child_view = !empty($form_state['values']) ? $form_state['values']['style_options']['child_view'] : $this->options['child_view'];
    if (!empty($child_view)) {
      list($child_view_id, $child_view_display) = explode('/', $child_view);
      $child_view = $views[$child_view_id];
      $child_view->build($child_view_display);
      if (!empty($child_view->build_info['fail'])) {
        $form['error_markup'] = array(
          '#markup' => '<div class="error messages">' . t('The selected view could not be built, possibly due to its argument configuration. Please select another view.') . '</div>',
          '#weight' => -1,
        );
        return;
      }

      $child_columns = $child_view->style_plugin->sanitize_columns($child_view->style_plugin->options['columns']);
      $child_field_names = $child_view->display_handler->get_field_labels();
      $child_column_labels = array();
      $has_common_columns = FALSE;
      // Filter out the fields that are shown in other columns.
      foreach ($child_columns as $field => $column) {
        if ($field == $column) {
          $child_column_labels[$column] = $child_field_names[$column];

          // For Field API fields, a specific column needs to be selected for merging.
          if (is_a($child_view->field[$field], 'views_handler_field_field')) {
            $field_columns = array_keys($child_view->field[$field]->field_info['columns']);
            foreach ($field_columns as $field_column) {
              $child_merge_column_labels[$column . '/' . $field_column] = $child_field_names[$column] . ' - ' . $field_column;
            }
          }
          else {
            $child_merge_column_labels[$column] = $child_field_names[$column];
          }

          if (isset($columns[$column])) {
            $has_common_columns = TRUE;
          }
        }
      }
      if ($has_common_columns) {
        $form['error_markup'] = array(
          '#markup' => '<div class="error messages">' . t('The selected child view has columns in common with the parent view. Please select another view.') . '</div>',
          '#weight' => -1,
        );
        return;
      }

      $form['column_mappings'] = array(
        '#type' => 'fieldset',
        '#title' => t('Column mappings'),
        '#weight' => -1,
      );
      $form['column_mappings']['merge_column_parent'] = array(
        '#type' => 'select',
        '#title' => t('Parent column to merge on'),
        '#options' => $parent_merge_column_labels,
        '#default_value' => $this->options['column_mappings']['merge_column_parent'],
      );
      $form['column_mappings']['merge_column_child'] = array(
        '#type' => 'select',
        '#title' => t('Child column to merge on'),
        '#options' => $child_merge_column_labels,
        '#default_value' => $this->options['column_mappings']['merge_column_child'],
      );

      $form['column_mappings']['help'] = array(
        '#type' => 'markup',
        '#markup' => '<p>Select how each of the columns of the parent view map to the columns of the child view.</p>',
      );
      foreach ($columns as $field => $column) {
        if ($field == $column) {
          $form['column_mappings'][$column] = array(
            '#type' => 'select',
            '#title' => $field_names[$column],
            '#options' => array(t('None')) + $child_column_labels,
            '#default_value' => $this->options['column_mappings'][$column],
          );
        }
      }

      $form['display_settings'] = array(
        '#type' => 'fieldset',
        '#title' => t('Child row display settings'),
        '#weight' => -1,
      );
      $form['display_settings']['collapsible'] = array(
        '#type' => 'checkbox',
        '#title' => t('Collapsible'),
        '#default_value' => $this->options['display_settings']['collapsible'],
      );
      $form['display_settings']['target_column'] = array(
        '#type' => 'select',
        '#title' => 'Target column',
        '#description' => t('The column where the link for collapsing the child rows will be placed'),
        '#options' => $parent_column_labels,
        '#default_value' => $this->options['display_settings']['target_column'],
        '#dependency' => array(
          'edit-style-options-display-settings-collapsible' => array(1)
        ),
      );
      $form['display_settings']['singular_count'] = array(
        '#type' => 'textfield',
        '#title' => 'Singular count',
        '#description' => t('format_plural() syntax. Used as a link to show / hide child rows.'),
        '#default_value' => $this->options['display_settings']['singular_count'],
        '#dependency' => array(
          'edit-style-options-display-settings-collapsible' => array(1)
        ),
      );
      $form['display_settings']['plural_count'] = array(
        '#type' => 'textfield',
        '#title' => t('Plural count'),
        '#description' => t('format_plural() syntax. Used as a link to show / hide child rows.'),
        '#default_value' => $this->options['display_settings']['plural_count'],
        '#dependency' => array(
          'edit-style-options-display-settings-collapsible' => array(1)
        ),
      );
      $form['display_settings']['collapsed'] = array(
        '#type' => 'checkbox',
        '#title' => t('Collapsed'),
        '#default_value' => $this->options['display_settings']['collapsed'],
      );
    }
  }

  function render_fields($result) {
    if (!$this->uses_fields()) {
      return;
    }

    if (!isset($this->rendered_fields)) {
      $this->rendered_fields = array();
      $this->view->row_index = 0;
      $keys = array_keys($this->view->field);
      foreach ($result as $count => $row) {
        $this->view->row_index = $count;
        foreach ($keys as $id) {
          if ($row->_type == $this->view->field[$id]->_type) {
            $this->rendered_fields[$count][$id] = $this->view->field[$id]->theme($row);
          }
          else {
            $this->rendered_fields[$count][$id] = '';
          }
        }

        $this->row_tokens[$count] = $this->view->field[$id]->get_render_tokens(array());
      }
      unset($this->view->row_index);
    }

    return $this->rendered_fields;
  }
}
