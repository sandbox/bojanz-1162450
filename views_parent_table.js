(function ($) {
  Drupal.behaviors.parentTable = {
    attach: function (context, settings) {
      $('.collapsible a', context).click(function() {
        var collapsible = $(this).closest('.collapsible')[0];
        var targetId = $(this).attr('rel');
        if (!$(collapsible).hasClass('collapsed')) {
          $(collapsible).addClass('collapsed');
          $('.child-row-' + targetId).hide();
        }
        else {
          $(collapsible).removeClass('collapsed');
          $('.child-row-' + targetId).show();
        }
        return false;
      });
    }
  }
})(jQuery);
